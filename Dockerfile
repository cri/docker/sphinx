FROM python:3-alpine

RUN apk --no-cache add make python3-dev

RUN python3 -m pip install sphinx sphinx_rtd_theme
